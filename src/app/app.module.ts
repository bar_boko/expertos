import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { ComponentsModule } from './components/components.module'
import { AppRootComponent } from './app-root/app-root.component'
import { MaterialModule } from '@exp-material'

@NgModule({
  declarations: [
  AppRootComponent,
],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppRootComponent],
})
export class AppModule { }
