import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { routes } from './routes'
import { SpellsModule } from './features/spells/spells.module'
import { StudentsModule } from './features/students/students.module'
import { HousesModule } from './features/houses/houses.module'

@NgModule({
  imports: [RouterModule.forRoot(routes),
    SpellsModule,
    StudentsModule,
    HousesModule,
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
