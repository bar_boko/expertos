import { Component, OnInit, Output, EventEmitter } from '@angular/core'
import { Route } from '@angular/router'
import { routes } from '@exp-app/routes'

@Component({
  selector: 'exp-sidenav-menu',
  templateUrl: './sidenav-menu.component.html',
  styleUrls: ['./sidenav-menu.component.scss'],
})
export class SidenavMenuComponent implements OnInit {
  @Output() public onSelectClick: EventEmitter<any> = new EventEmitter()

  public menuRoutes: Route[]

  public ngOnInit(): void {
    this.menuRoutes = [...routes.filter(x => x.path && x.data?.icon)]
  }

  public invokeSelectClick(): void {
    this.onSelectClick.emit()
  }
}
