import { Component, Input, Optional } from '@angular/core'

@Component({
  selector: 'exp-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.scss'],
})
export class FabComponent {
  @Optional() @Input() public icon: string = 'add'
  @Input() public target: string
}
