import { Component, EventEmitter, Output } from '@angular/core'

@Component({
  selector: 'exp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Output() public onMenuButtonClick: EventEmitter<any> = new EventEmitter()

  public invokeMenuButtonClick(): void {
    this.onMenuButtonClick.emit()
  }
}
