import { NgModule } from '@angular/core'
import { MaterialModule } from '@exp-material'
import { SidenavMenuComponent } from './sidenav-menu/sidenav-menu.component'
import { FabComponent } from './fab/fab.component'
import { TitleComponent } from './title/title.component'
import { TitleService } from './title/title.service'
import { HeaderComponent } from './header/header.component'
import { FiltersToolbarComponent } from './filters-toolbar/filters-toolbar.component'
import { ContainerComponent } from './container/container.component'

@NgModule({
  declarations: [
    SidenavMenuComponent,
    FabComponent,
    TitleComponent,
    HeaderComponent,
    FiltersToolbarComponent,
    ContainerComponent,
  ],
  imports: [
    MaterialModule,
  ],
  providers: [
    TitleService,
  ],
  exports: [
    SidenavMenuComponent,
    FabComponent,
    TitleComponent,
    HeaderComponent,
    FiltersToolbarComponent,
    ContainerComponent,
  ],
})
export class ComponentsModule { }
