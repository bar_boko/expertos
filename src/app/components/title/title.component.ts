import { Component } from '@angular/core'
import { TitleService } from './title.service'

@Component({
  selector: 'exp-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss'],
})
export class TitleComponent {
  constructor(public readonly titleService: TitleService) {
  }
}
