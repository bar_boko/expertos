import { Injectable } from '@angular/core'
import { Observable, of, combineLatest } from 'rxjs'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'
import { filter, distinctUntilChanged, map, startWith } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class TitleService {
  public readonly title: Observable<string>

  constructor(private readonly router: Router, private readonly activatedRoute: ActivatedRoute) {
    this.title = this.createTitleObservable()
  }

  private createTitleObservable(): Observable<string> {
    const routerObservable: Observable<ActivatedRoute> = this.router.events.pipe(
      startWith(null),
      filter(event => event instanceof NavigationEnd),
      distinctUntilChanged(),
      map(x => this.activatedRoute),
    )

    return routerObservable.pipe(
      startWith(this.activatedRoute),
      map(activatedRoute => activatedRoute?.firstChild?.routeConfig?.data?.label ?? ''),
    )
  }
}
