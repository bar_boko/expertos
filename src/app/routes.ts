import { Type } from '@angular/core'
import { Route, Routes } from '@angular/router'
import { SpellsViewComponent } from './features/spells/views/spells-view/spells-view.component'
import { NewSpellViewComponent } from './features/spells/views/new-spell-view/new-spell-view.component'
import { StudentsViewComponent } from './features/students/views/students-view/students-view.component'
import { NewStudentViewComponent } from './features/students/views/new-student-view/new-student-view.component'
import { EditStudentViewComponent } from './features/students/views/edit-student-view/edit-student-view.component'
import { HousesViewComponent } from './features/houses/views/houses-view/houses-view.component'
import { NewHouseViewComponent } from './features/houses/views/new-house-view/new-house-view.component'

const createRoute = (path: string, component: Type<any>,
                     label: string, icon?: string): Route => ({
    path,
    component,
    data: {
        label,
        icon,
    },
})

const createRedirect = (path: string, redirect: string): Route => ({
    path,
    redirectTo: redirect,
    pathMatch: 'full',
})

export const routes: Routes = [
    createRoute('spells', SpellsViewComponent, 'כשפים', 'extension'),
    createRoute('spells/new', NewSpellViewComponent, 'הוספת כישוף'),
    createRoute('students', StudentsViewComponent, 'תלמידי בית הספר', 'person'),
    createRoute('students/new', NewStudentViewComponent, 'רישום תלמיד חדש לבית הספר'),
    createRoute('students/:id', EditStudentViewComponent, 'עריכת פרטי תלמיד'),
    createRoute('houses', HousesViewComponent, 'בתים', 'home'),
    createRoute('houses/new', NewHouseViewComponent, 'הוספת בית חדש'),
    createRedirect('', 'students'),
]
