import { IdentityObject } from './identity-object'
import { Color } from './enums/color.enum'
import { Icon } from './enums/icon.enum'

export interface House extends IdentityObject {
    name: string
    color: Color
    icon: Icon
}
