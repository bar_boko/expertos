import { IdentityObject } from './identity-object'
import { Gender } from './enums/gender.enum'
import { House } from './house'
import { Ref } from './ref'
import { Spell } from './spell'

export interface Student extends IdentityObject {
    firstName: string
    lastName: string
    birthDate: Date
    registrationDate: Date
    house: Ref<House>
    spells?: Ref<Spell>[]
    gender: Gender
}
