import { IdentityObject } from './identity-object'

export type Ref<T extends IdentityObject> = number

export function getRef<T extends IdentityObject>(obj: T): Ref<T> | null {
    return obj?.id ?? null
}
