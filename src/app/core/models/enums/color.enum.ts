import { EnumDictionary } from './enum-dictionary.type'

export enum Color {
    Blue = 'blue',
    Green = 'green',
    Yellow = 'yellow',
    Red = 'red',
    Black = 'black',
    White = 'white',
    Orange = 'orange',
    Pink = 'pink',
    Purple = 'purple',
}

export const Colors: EnumDictionary<Color, string> = {
    [Color.Blue]: 'כחול',
    [Color.Green]: 'ירוק',
    [Color.Yellow]: 'צהוב',
    [Color.Red]: 'אדום',
    [Color.Black]: 'שחור',
    [Color.White]: 'לבן',
    [Color.Orange]: 'כתום',
    [Color.Pink]: 'ורוד',
    [Color.Purple]: 'סגול',
}
