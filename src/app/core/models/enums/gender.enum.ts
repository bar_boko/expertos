import { EnumDictionary } from './enum-dictionary.type'

export enum Gender {
    Male = 'male',
    Female = 'female',
    Queer = 'queer',
}

export const GenderTitles: EnumDictionary<Gender, string> = {
    [Gender.Male]: 'זכר',
    [Gender.Female]: 'נקבה',
    [Gender.Queer]: 'קוויר',
}
