import { EnumDictionary } from './enum-dictionary.type'
export enum Icon {
    Favorite = 'favorite',
    ChangeHistory = 'change_history',
    Eco = 'eco',
    Explore = 'explore',
    Face = 'face',
    Fingerprint = 'fingerprint',
    Grade = 'grade',
    Weekend = 'weekend',
    Waves = 'waves',
    Adjust = 'adjust',
}

export const Icons: EnumDictionary<Icon, string> = {
    [Icon.Favorite]: 'לב',
    [Icon.ChangeHistory]: 'משולש',
    [Icon.Eco]: 'עלה',
    [Icon.Explore]: 'מצפן',
    [Icon.Face]: 'פרצוף',
    [Icon.Fingerprint]: 'טביעת אצבע',
    [Icon.Grade]: 'כוכב',
    [Icon.Weekend]: 'ספה',
    [Icon.Waves]: 'גלים',
    [Icon.Adjust]: 'מרכז',
}
