import { IdentityObject } from './identity-object'

export interface Spell extends IdentityObject {
    name: string
    description: string
}
