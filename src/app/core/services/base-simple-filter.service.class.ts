import { FormControl, FormBuilder } from '@angular/forms'
import { Subject, BehaviorSubject, Observable } from 'rxjs'
import { startWith, mergeMap, map } from 'rxjs/operators'

export abstract class BaseSimpleFilterService<T> {
    public readonly query: FormControl
    private readonly values: Subject<T[]>

    constructor(private readonly formBuilder: FormBuilder) {
        this.query = this.createQueryControl()
        this.values = new BehaviorSubject([])
    }

    public setValues(values: T[]) {
        this.values.next(values)
    }

    public createQueryControl(): FormControl {
        return this.formBuilder.control('', [])
    }

    public asObservable(): Observable<string> {
        return this.query.valueChanges
    }

    public get filteredValues(): Observable<T[]> {
        return this.asObservable().pipe(
            startWith(''),
            mergeMap(query => this.values.pipe(
              map(values => values.filter(value => this.filterBy(value, query))),
            ),
        ))
    }

    protected abstract filterBy(value: T, query: string): boolean
}
