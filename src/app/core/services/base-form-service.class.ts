import { FormGroup, FormBuilder, FormControl, FormArray, AbstractControl } from '@angular/forms'
import { Observable } from 'rxjs'

export abstract class BaseFormService<T> {
    public get isValid(): boolean {
        return this.formGroup.valid
    }
    public readonly formGroup: FormGroup

    constructor(protected readonly formBuilder: FormBuilder) {
        this.formGroup = this.createFormGroup()
    }

    public getValue<T>(name: string): T {
        return this.formGroup.get(name)?.value
    }

    public getValueAsObservable<T>(name: string): Observable<T> {
        return this.formGroup.get(name)?.valueChanges as Observable<T>
    }

    public getControl(name: string): FormControl {
        return this.get<FormControl>(name)
    }

    public getGroup(name: string): FormGroup {
        return this.get<FormGroup>(name)
    }

    public getArray(name: string): FormArray {
        return this.get<FormArray>(name)
    }

    public hasError(name: string, validation: string): boolean {
        return this.formGroup.get(name)?.hasError(validation) ?? false
    }

    public hasAnyError(name: string): boolean {
        return !!this.formGroup.get(name)?.errors ?? false
    }

    public abstract fromObject(data: T): void
    public abstract toObject(): T
    protected abstract createFormGroup(): FormGroup

    private get<T extends AbstractControl>(name: string): T {
        return this.formGroup.get(name) as T
    }
}
