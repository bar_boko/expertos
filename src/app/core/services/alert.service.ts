import { Injectable } from '@angular/core'
import { MatSnackBar } from '@angular/material/snack-bar'
import { environment } from '@exp-env'
import { Direction } from '@angular/cdk/bidi'

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private readonly snackBar: MatSnackBar) {
  }

  public push(message: string): void {
    this.snackBar.open(message, 'סמן כנקרא', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
      direction: this.getDirection(environment.langDirection),
    })
  }

  private getDirection(direction: string): Direction {
    return direction === 'rtl' ? 'rtl' : 'ltr'
  }
}
