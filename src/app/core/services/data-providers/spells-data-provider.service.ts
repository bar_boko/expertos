import { Injectable } from '@angular/core'
import { BaseDataProviderService } from './base-data-provider.service'
import { Spell } from '@exp-app/core/models/spell'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root',
})
export class SpellsDataProviderService extends BaseDataProviderService<Spell> {
  constructor(client: HttpClient) {
    super(client, 'spells')
  }
}
