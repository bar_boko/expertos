import { Injectable } from '@angular/core'
import { BaseDataProviderService } from './base-data-provider.service'
import { Student } from '@exp-app/core/models/student'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root',
})
export class StudentsDataProviderService extends BaseDataProviderService<Student> {
  constructor(client: HttpClient) {
    super(client, 'students')
  }
}
