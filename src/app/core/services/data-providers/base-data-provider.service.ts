import { HttpClient } from '@angular/common/http'
import { environment } from '@exp-env'
import { IdentityObject } from '@exp-app/core/models/identity-object'

export abstract class BaseDataProviderService<T extends IdentityObject> {
  protected readonly url: string

  constructor(
    protected readonly client: HttpClient,
    baseRoute: string,
  ) {
    this.url = `${environment.backendApiUri}/${baseRoute}`
  }

  public async getItems(): Promise<T[]> {
    return await this.client.get<T[]>(this.url).toPromise()
  }

  public async addNewItem(item: T): Promise<any> {
    return await this.client.post<any>(this.url, item).toPromise()
  }

  public async deleteItem(id: number): Promise<any> {
    return await this.client.delete<any>(`${this.url}/${id}`).toPromise()
  }

  public async getItemById(id: number): Promise<T> {
    return await this.client.get<T>(`${this.url}/${id}`).toPromise()
  }

  public async updateItem(item: T): Promise<any> {
    return await this.client.put<any>(`${this.url}/${item.id}`, item).toPromise()
  }
}
