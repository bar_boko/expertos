import { Injectable } from '@angular/core'
import { BaseDataProviderService } from './base-data-provider.service'
import { House } from '@exp-app/core/models/house'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root',
})
export class HousesDataProviderService extends BaseDataProviderService<House> {
  constructor(client: HttpClient) {
    super(client, 'houses')
  }
}
