import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HousesDataProviderService } from './services/data-providers/houses-data-provider.service'
import { SpellsDataProviderService } from './services/data-providers/spells-data-provider.service'
import { StudentsDataProviderService } from './services/data-providers/students-data-provider.service'
import { AlertService } from './services/alert.service'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { HttpClientModule } from '@angular/common/http'
import { RouterModule } from '@angular/router'

@NgModule({
  declarations: [],
  imports: [
CommonModule,
    MatSnackBarModule,
    HttpClientModule,
    RouterModule,
  ],
  providers: [
    AlertService,
    HousesDataProviderService,
    SpellsDataProviderService,
    StudentsDataProviderService,
  ],
})
export class CoreModule { }
