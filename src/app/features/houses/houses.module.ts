import { NgModule } from '@angular/core'
import { HousesViewComponent } from './views/houses-view/houses-view.component'
import { NewHouseViewComponent } from './views/new-house-view/new-house-view.component'
import { ComponentsModule } from 'src/app/components/components.module'
import { CoreModule } from 'src/app/core/core.module'
import { MaterialModule } from '@exp-material'

@NgModule({
  declarations: [
    HousesViewComponent,
    NewHouseViewComponent,
  ],
  imports: [
    ComponentsModule,
    CoreModule,
    MaterialModule,
  ],
  exports: [
    HousesViewComponent,
    NewHouseViewComponent,
  ],
})
export class HousesModule {}
