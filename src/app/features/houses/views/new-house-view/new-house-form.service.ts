import { Injectable } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Color, Colors } from '@exp-app/core/models/enums/color.enum'
import { Icon, Icons } from '@exp-app/core/models/enums/icon.enum'
import { House } from '@exp-app/core/models/house'
import { EnumDictionary } from '@exp-app/core/models/enums/enum-dictionary.type'
import { BaseFormService } from '@exp-core/services/base-form-service.class'

const { minLength, maxLength, required } = Validators

export const NAME = 'name'
export const ICON = 'icon'
export const COLOR = 'color'

@Injectable()
export class NewHouseFormService extends BaseFormService<House> {
    get name(): string {
        return this.getValue(NAME)
    }

    get icon(): Icon {
        return this.getValue(ICON)
    }

    get iconTitle(): string {
        return this.icons[this.icon]
    }

    get color(): Color {
        return this.getValue(COLOR)
    }

    get colorTitle(): string {
        return this.colors[this.color]
    }

    public readonly colors: EnumDictionary<Color, string> = Colors
    public readonly icons: EnumDictionary<Icon, string> = Icons
    public readonly formGroup: FormGroup

    public constructor(formBuilder: FormBuilder) {
        super(formBuilder)
    }

    public toObject(): House {
        return ({
            id: 0,
            ...this.formGroup.value,
        })
    }

    public fromObject(data: House): void {
        const { name, icon, color } = data

        this.formGroup.patchValue({
            name, icon, color,
        })
    }

    protected createFormGroup(): FormGroup {
        return this.formBuilder.group({
            [NAME]: ['', [required, minLength(2), maxLength(25)]],
            [ICON]: [Icon.Eco, required],
            [COLOR]: [Color.Red, required],
        })
    }
}
