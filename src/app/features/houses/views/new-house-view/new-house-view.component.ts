import { Component } from '@angular/core'
import { House } from '@exp-app/core/models/house'
import { HousesDataProviderService } from '@exp-app/core/services/data-providers/houses-data-provider.service'
import { AlertService } from '@exp-core/services/alert.service'
import { Router } from '@angular/router'
import { NewHouseFormService } from './new-house-form.service'

@Component({
  selector: 'exp-new-house-view',
  templateUrl: './new-house-view.component.html',
  styleUrls: ['./new-house-view.component.scss'],
  providers: [NewHouseFormService],
})
export class NewHouseViewComponent {
  constructor(
    private readonly dataProvider: HousesDataProviderService,
    private readonly alertService: AlertService,
    private readonly router: Router,
    public readonly formService: NewHouseFormService,
  ) {}

  public async onSubmit(): Promise<any> {
    const newHouse: House = this.formService.toObject()

    try {
      const values = await this.dataProvider.addNewItem(newHouse)
      this.alertService.push(`הבית ׳${newHouse.name}׳ נוסף בהצלחה`)
      this.router.navigate(['/houses'])
      return Promise.resolve(values)
    } catch (error) {
      this.alertService.push('לא היה ניתן להוסיף את הבית למערכת')
      return Promise.reject(error)
    }
  }
}
