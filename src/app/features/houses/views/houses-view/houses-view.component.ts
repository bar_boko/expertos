import { Component, OnInit } from '@angular/core'
import { AlertService } from '@exp-app/core/services/alert.service'
import { HousesDataProviderService } from '@exp-app/core/services/data-providers/houses-data-provider.service'
import { House } from '@exp-app/core/models/house'
import { HousesViewFilterService } from './houses-view-filter.service'
import { Observable } from 'rxjs'

@Component({
  selector: 'exp-houses-view',
  templateUrl: './houses-view.component.html',
  styleUrls: ['./houses-view.component.scss'],
  providers: [HousesViewFilterService],
})
export class HousesViewComponent implements OnInit {
  public filteredHouses: Observable<House[]>

  constructor(
    private readonly alertService: AlertService,
    private readonly dataProvider: HousesDataProviderService,
    public readonly filterService: HousesViewFilterService,
  ) {
  }

  public ngOnInit(): void {
    this.dataProvider.getItems()
      .then(data => this.filterService.setValues(data))
      .catch(() => this.alertService.push(
        'לא ניתן לקבל מהשרת את נתוני הבתים',
      ))

    this.filteredHouses = this.filterService.filteredValues
  }
}
