import { Injectable } from '@angular/core'
import { FormBuilder } from '@angular/forms'
import { House } from '@exp-app/core/models/house'
import { BaseSimpleFilterService } from '@exp-core/services/base-simple-filter.service.class'

@Injectable()
export class HousesViewFilterService extends BaseSimpleFilterService<House> {
    constructor(formBuilder: FormBuilder) {
        super(formBuilder)
    }

    protected filterBy(value: House, query: string): boolean {
        return value?.name?.includes(query)
    }
}
