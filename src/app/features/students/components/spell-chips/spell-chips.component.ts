import { Component, OnInit, Input, forwardRef, ViewChild,
  OnChanges, SimpleChanges, ElementRef, OnDestroy } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormControl,
  FormControlDirective, ControlContainer } from '@angular/forms'
import { Spell } from '@exp-app/core/models/spell'
import { SpellChipsService } from './spell-chips.service'
import { COMMA, ENTER } from '@angular/cdk/keycodes'
import { AlertService } from '@exp-app/core/services/alert.service'
import { MatChipInputEvent } from '@angular/material/chips'
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete'
import { Ref } from '@exp-app/core/models/ref'
import { Subscription } from 'rxjs'

const CUSTOM_VALUE_ACCESSOR: any = {
  provide : NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SpellChipsComponent),
  multi : true,
}

@Component({
  selector: 'exp-spell-chips',
  templateUrl: './spell-chips.component.html',
  styleUrls: ['./spell-chips.component.scss'],
  providers: [SpellChipsService, CUSTOM_VALUE_ACCESSOR],
})
export class SpellChipsComponent implements OnInit, OnChanges, OnDestroy, ControlValueAccessor {
  @Input()
  public set spells(spells: Spell[]) {
    this.chipsService.spells.next(spells ?? [])
  }

  public get currentFormControl(): FormControl {
    return this.formControl ||
      this.controlContainer.control?.get(this.formControlName)
  }

  @ViewChild(FormControlDirective, { static: true }) public formControlDirective: FormControlDirective
  @ViewChild('queryInput') public queryInput: ElementRef
  @Input() public formControl: FormControl
  @Input() public formControlName: string

  public readonly seperators: number[] = [COMMA, ENTER]
  private queryWasResettedSubscription: Subscription

  constructor(public readonly chipsService: SpellChipsService,
              private readonly alertService: AlertService,
              private readonly controlContainer: ControlContainer) {
  }

  public ngOnDestroy(): void {
    this.queryWasResettedSubscription?.unsubscribe()
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (!changes.formControlName && !changes.formControl) {
      return
    }

    this.setFormControl()
  }

  public writeValue(obj: Spell[]): void {
    this.formControlDirective?.valueAccessor?.writeValue(obj)
  }

  public registerOnChange(fn: any): void {
    this.formControlDirective?.valueAccessor?.registerOnChange(fn)
  }

  public registerOnTouched(fn: any): void {
    this.formControlDirective?.valueAccessor?.registerOnTouched(fn)
  }

  public setDisabledState(disabled: boolean): void {
    const setDisabledState = this.formControlDirective?.valueAccessor?.setDisabledState
    if (!setDisabledState) {
      return
    }

    setDisabledState(disabled)
  }

  public ngOnInit(): void {
    this.queryWasResettedSubscription = this.chipsService.queryWasResetted.subscribe(() => {
      this.resetQuery()
    })
  }

  public selected(event: MatAutocompleteSelectedEvent): void {
    const selectedSpell: Ref<Spell> = event.option?.value
    if (!selectedSpell) {
      this.alertService.push('הכישוף שנבחר לא נמצא ברשימה')
      return
    }

    this.chipsService.add(selectedSpell)
  }

  public remove(spell: Spell): void {
    const selectedSpell: Ref<Spell> = spell?.id
    if (!selectedSpell) {
      this.alertService.push('הכישוף שנבחר לא נמצא ברשימה')
      return
    }

    this.chipsService.remove(selectedSpell)
  }

  public onQueryAdd(event: MatChipInputEvent): void {
    if (!event.value || typeof event.value !== 'string') {
      return
    }

    this.alertService.push('לא ניתן להוסיף כישוף שלא מופיע ברשימה')
  }

  private resetQuery(): void {
    if (!this.queryInput.nativeElement?.value) {
      return
    }

    this.queryInput.nativeElement.value = ''
  }

  private setFormControl(): void {
    this.chipsService.valueControl = this.currentFormControl
  }
}
