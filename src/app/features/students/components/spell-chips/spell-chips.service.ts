import { Injectable, EventEmitter } from '@angular/core'
import { BehaviorSubject, Subject, of, Observable } from 'rxjs'
import { Spell } from '@exp-app/core/models/spell'
import { FormBuilder, FormControl } from '@angular/forms'
import { startWith, mergeMap, map, filter, flatMap } from 'rxjs/operators'
import { Ref } from '@exp-app/core/models/ref'
import { differenceBy } from 'lodash'

const DEFAULT_VALUE: Ref<Spell>[] = []
const DEFAULT_QUERY: string = ''

type SpellQueryFilter = (spell: Spell, query: string) => boolean
type SpellSelectedFilter = (spell: Spell, selectedSpells: Ref<Spell>[]) => boolean

@Injectable()
export class SpellChipsService {
    public readonly spells: Subject<Spell[]> = new BehaviorSubject([])
    public readonly queryControl: FormControl = new FormControl(DEFAULT_QUERY)
    public readonly queryWasResetted: EventEmitter<string> = new EventEmitter()
    public autocompleteSpells: Observable<Spell[]> = of([])
    public selectedSpells: Observable<Spell[]> = of([])
    private _valueControl: FormControl = new FormControl(DEFAULT_VALUE)

    public get valueControl(): FormControl {
        return this._valueControl
    }

    public set valueControl(newControl: FormControl) {
        this._valueControl = newControl
        this.updateObservables()
    }

    constructor() {
        this.updateObservables()
    }

    public add(spell: Ref<Spell>): void {
        const value = [...this.valueControl.value]

        if (value.includes(spell)) {
            return
        }

        value.push(spell)
        this.valueControl.setValue(value)
        this.resetQuery()
    }

    public remove(spell: Ref<Spell>): void {
        let value: Ref<Spell>[] = this.valueControl.value

        if (!value.includes(spell)) {
            return
        }

        value = [...value.filter(valueSpell => valueSpell !== spell)]
        this.valueControl.setValue(value)
        this.resetQuery()
    }

    public resetQuery(): void {
        this.queryControl.setValue(DEFAULT_QUERY, {emitEvent: true})
        this.queryWasResetted.emit(this.queryControl.value)
    }

    private updateObservables(): void {
        this.selectedSpells = this.createSelectedSpellsObservable()
        this.autocompleteSpells = this.createAutocompleteSpellsObservable()
    }

    private createAutocompleteSpellsObservable(): Observable<Spell[]> {
        const combinedFilter: SpellQueryFilter = (spell, query) =>
            spell?.name?.includes(query) || spell?.description?.includes(query)

        return this.selectedSpells.pipe(
            startWith([]),
            mergeMap(selected => this.spells.pipe(
                startWith([]),
                map(spells => differenceBy(spells, selected, 'id')),
            )),
            mergeMap(notSelectedSpells => this.queryControl.valueChanges.pipe(
                startWith(''),
                filter(x => typeof x === 'string'),
                map(query => notSelectedSpells.filter(notSelectedSpell => combinedFilter(notSelectedSpell, query))),
            )),
        )
    }

    private createSelectedSpellsObservable(): Observable<Spell[]> {
        const combinedFilter: SpellSelectedFilter = (spell, selectedSpells) =>
            spell && (selectedSpells ?? []).includes(spell.id)

        return this.valueControl.valueChanges.pipe(
            startWith([]),
            mergeMap(chosenSpells => this.spells.pipe(
                    map(spells => spells.filter(
                            spell => combinedFilter(spell, chosenSpells))),
                ),
            ),
        )
    }
}
