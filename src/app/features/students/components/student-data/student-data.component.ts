import { Component, OnInit, Input } from '@angular/core'
import { SpellsDataProviderService } from '@exp-core/services/data-providers/spells-data-provider.service'
import { HousesDataProviderService } from '@exp-core/services/data-providers/houses-data-provider.service'
import { AlertService } from '@exp-core/services/alert.service'
import { Router } from '@angular/router'
import { House } from '@exp-core/models/house'
import { Spell } from '@exp-core/models/spell'
import { Gender, GenderTitles } from '@exp-core/models/enums/gender.enum'
import { EnumDictionary } from '@exp-core/models/enums/enum-dictionary.type'
import { BaseStudentForm, HOUSE } from '@exp-features/students/components/student-data/base-students-form-service.class'
import { BehaviorSubject, Subject, Observable } from 'rxjs'
import { flatMap, startWith, map, mergeMap, first, filter } from 'rxjs/operators'
import { Ref } from '@exp-core/models/ref'

@Component({
  selector: 'exp-student-data',
  templateUrl: './student-data.component.html',
  styleUrls: ['./student-data.component.scss'],
})
export class StudentDataComponent implements OnInit {
  @Input() public formService: BaseStudentForm

  public readonly genders: EnumDictionary<Gender, string> = GenderTitles
  public spells: Subject<Spell[]> = new BehaviorSubject([])
  public houses: Subject<House[]> = new BehaviorSubject([])
  public selectedHouse: Observable<House>

  constructor(
    private readonly spellsDataProvider: SpellsDataProviderService,
    private readonly housesDataProvider: HousesDataProviderService,
    private readonly alertService: AlertService,
    private readonly router: Router,
  ) {}

  public ngOnInit(): void {
    this.spellsDataProvider
      .getItems()
      .then(values => this.spells.next(values))
      .catch(error => this.handleFailGetAllRequest(error, 'כשפים'))

    this.housesDataProvider
      .getItems()
      .then(values => {
        this.houses.next(values)
      })
      .catch(error => this.handleFailGetAllRequest(error, 'בתים'))

    this.selectedHouse = this.getSelectedHouse()
  }

  private getSelectedHouse(): Observable<House> {
    return this.formService.getValueAsObservable(HOUSE).pipe(
      startWith(0),
      map(x => x as Ref<House>),
      mergeMap(houseRef => this.houses.pipe(
        startWith([]),
        flatMap(x => x),
        filter(house => house.id === houseRef),
        first(),
      )))
  }

  private handleFailGetAllRequest(error: any, name: string): Promise<any> {
    this.alertService.push(`חלה שגיאה בקבלת נתוני ה${name} מהמערכת`)
    this.router.navigate(['/students'])
    return Promise.reject(error)
  }
}
