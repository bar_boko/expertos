import { FormGroup, Validators, ValidatorFn } from '@angular/forms'
import { Student } from '@exp-app/core/models/student'
import { BaseFormService } from '@exp-core/services/base-form-service.class'
import { Gender } from '@exp-app/core/models/enums/gender.enum'

const {required, minLength, maxLength} = Validators

export const FIRST_NAME = 'firstName'
export const LAST_NAME = 'lastName'
export const BIRTH_DATE = 'birthDate'
export const REGISTRATION_DATE = 'registrationDate'
export const HOUSE = 'house'
export const SPELLS = 'spells'
export const GENDER = 'gender'

export function onlyPast(date: Date): ValidatorFn {
    return control => {
        const currentDate = (control?.value as Date)?.getDate()
        if (!currentDate) {
            return {emptyDate: true}
        }

        if (currentDate > date.getDate()) {
            return {future: true}
        }

        return null
    }
}

export abstract class BaseStudentForm extends BaseFormService<Student> {
    public fromObject(data: Student): void {
        data.registrationDate = new Date(data.registrationDate)
        data.birthDate = new Date(data.birthDate)
        this.formGroup.patchValue(data)
    }

    public toObject(): Student {
        return this.formGroup.value
    }

    protected createFormGroup(): FormGroup {
        return this.formBuilder.group({
            [FIRST_NAME]: ['', [required, minLength(2), maxLength(25)]],
            [LAST_NAME]: ['', [required, minLength(2), maxLength(25)]],
            [BIRTH_DATE]: [new Date(), [required, onlyPast(new Date())]],
            [HOUSE]: [null, [required]],
            [SPELLS]: [[], []],
            [GENDER]: [Gender.Male, [required]],
        },
        {updateOn: 'blur'})
    }
}
