import { Component } from '@angular/core'

@Component({
  selector: 'exp-student-grid-item',
  templateUrl: './student-grid-item.component.html',
  styleUrls: ['./student-grid-item.component.scss'],
})
export class StudentGridItemComponent {
}
