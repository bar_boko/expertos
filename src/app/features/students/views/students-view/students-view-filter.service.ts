import { Injectable } from '@angular/core'
import { FormGroup, FormBuilder, FormControl } from '@angular/forms'
import { BehaviorSubject, Subject, Observable, combineLatest } from 'rxjs'
import { Student } from '@exp-app/core/models/student'
import { startWith, mergeMap, map } from 'rxjs/operators'
import { Gender } from '@exp-app/core/models/enums/gender.enum'
import { Ref } from '@exp-app/core/models/ref'
import { House } from '@exp-app/core/models/house'

export const QUERY = 'query'
export const GENDER = 'gender'
export const HOUSE = 'house'

export const DEFAULT_QUERY: string = ''
export const DEFAULT_GENDER = null
export const DEFAULT_HOUSE = null

@Injectable()
export class StudentsViewFilterService {
    public get query(): FormControl {
        return this.getControl(QUERY)
    }

    public get gender(): FormControl {
        return this.getControl(GENDER)
    }

    public get house(): FormControl {
        return this.getControl(HOUSE)
    }

    public static filterStudentsByQuery(student: Student, query: string): boolean {
        return student?.firstName?.includes(query) || student?.lastName?.includes(query)
    }

    public static filterStudentsByGender(student: Student, gender: Gender): boolean {
        return student?.gender === gender
    }

    public static filterStudentsByHouse(student: Student, house: Ref<House>): boolean {
        return student?.house === house
    }

    public readonly formGroup: FormGroup
    public readonly students: Subject<Student[]> = new BehaviorSubject([])
    public readonly filteredStudents: Observable<Student[]>

    public constructor(private readonly formBuilder: FormBuilder) {
        this.formGroup = this.createFormGroup()
        this.filteredStudents = this.createFilteredStudents()
    }

    public setStudent(students: Student[]): void {
        this.students.next(students)
    }

    public resetFilters(): void {
        this.formGroup.patchValue({
            [QUERY]: DEFAULT_QUERY,
            [GENDER]: DEFAULT_GENDER,
            [HOUSE]: DEFAULT_HOUSE,
        })
    }

    private getControl(field: string): FormControl {
        return this.formGroup.get(field) as FormControl
    }

    private createFormGroup(): FormGroup {
        return this.formBuilder.group({
            [QUERY]: [DEFAULT_QUERY],
            [GENDER]: [DEFAULT_GENDER],
            [HOUSE]: [DEFAULT_HOUSE],
        })
    }

    private createFilteredStudents(): Observable<Student[]> {
        return combineLatest(
            this.students.pipe(startWith([])),
            this.query.valueChanges.pipe(startWith(DEFAULT_QUERY)),
            this.gender.valueChanges.pipe(startWith(DEFAULT_GENDER)),
            this.house.valueChanges.pipe(startWith(DEFAULT_HOUSE)),
        ).pipe(map(value => {
            const [students, query, gender, house] = value

            let resultStudents: Student[] = students ?? []
            if (query) {
                resultStudents = resultStudents.filter(
                    student => StudentsViewFilterService.filterStudentsByQuery(student, query))
            }

            if (gender) {
                resultStudents = resultStudents.filter(
                    student => StudentsViewFilterService.filterStudentsByGender(student, gender))
            }

            if (house) {
                resultStudents = resultStudents.filter(
                    student => StudentsViewFilterService.filterStudentsByHouse(student, house))
            }

            return resultStudents
        }))
    }
}
