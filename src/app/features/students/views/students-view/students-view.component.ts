import { Component, OnInit } from '@angular/core'
import { StudentsViewFilterService } from './students-view-filter.service'
import { AlertService } from '@exp-core/services/alert.service'
import { StudentsDataProviderService } from '@exp-core/services/data-providers/students-data-provider.service'
import { GenderTitles } from '@exp-core/models/enums/gender.enum'
import { HousesDataProviderService } from '@exp-core/services/data-providers/houses-data-provider.service'
import { BehaviorSubject, Subject, of, Observable } from 'rxjs'
import { House } from '@exp-app/core/models/house'
import { startWith, filter, map, mergeMap, first, flatMap } from 'rxjs/operators'
import { Ref } from '@exp-app/core/models/ref'
import { Student } from '@exp-app/core/models/student'
import { Router } from '@angular/router'
import { Nullable } from '@exp-app/core/models/nullable'

@Component({
  selector: 'exp-students-view',
  templateUrl: './students-view.component.html',
  styleUrls: ['./students-view.component.scss'],
  providers: [StudentsViewFilterService],
})
export class StudentsViewComponent implements OnInit {
  public readonly genders = GenderTitles
  public readonly houses: Subject<House[]> = new BehaviorSubject([])
  public selectedHouse: Observable<Nullable<House>> = of(null)

  constructor(
    public readonly filterService: StudentsViewFilterService,
    public readonly alertService: AlertService,
    public readonly router: Router,
    public readonly studentsDataProvider: StudentsDataProviderService,
    public readonly housesDataProvider: HousesDataProviderService,
  ) {
    this.selectedHouse = this.createSelectedHouse()
  }

  public ngOnInit(): void {
    this.getStudents()
    this.getHouses()
  }

  public getStudentHouseColor(student: Student): Observable<string> {
    return this.houses.pipe(
      startWith([]),
      flatMap(x => x),
      first(house => house.id === student.house),
      map(({color}) => color),
    )
  }

  public editStudent(student: Student): void {
    this.router.navigate(['/students', student.id])
  }

  private async getHouses(): Promise<House[]> {
    try {
      const houses = await this.housesDataProvider.getItems()
      this.houses.next(houses)
      return Promise.resolve(houses)
    } catch (error) {
      this.alertService.push('לא ניתן זמנית לסנן תלמידים לפי בתים')
      return Promise.reject(error)
    }
  }

  private async getStudents(): Promise<Student[]> {
    try {
      const students = await this.studentsDataProvider.getItems()
      this.filterService.students.next(students)
      return Promise.resolve(students)
    } catch (error) {
      this.alertService.push('חלה שגיאה בעת קבלת נתוני התלמידים')
      return Promise.reject(error)
    }
  }

  private createSelectedHouse(): Observable<House> {
    return this.filterService.house.valueChanges.pipe(
      startWith(null),
      filter(x => x !== null),
      map(x => x as Ref<House>),
      mergeMap(houseRef => this.houses.pipe(
        flatMap(x => x),
        filter(house => house.id === houseRef),
        first(),
      )),
    )
  }
}
