import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Student } from '@exp-app/core/models/student'
import { AlertService } from '@exp-app/core/services/alert.service'
import { StudentsDataProviderService } from '@exp-app/core/services/data-providers/students-data-provider.service'
import { EditStudentFormService } from './edit-student-form.service'

@Component({
  selector: 'exp-edit-student-view',
  templateUrl: './edit-student-view.component.html',
  styleUrls: ['./edit-student-view.component.scss'],
  providers: [EditStudentFormService],
})
export class EditStudentViewComponent implements OnInit {
  public static readonly idRegex = /^\d{1,}$/g
  private student: Student

  constructor(
    private readonly studentDataProvider: StudentsDataProviderService,
    private readonly alertService: AlertService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    public readonly formService: EditStudentFormService,
  ) {}

  public ngOnInit(): void {
    const id: string | null = this.activatedRoute.snapshot.paramMap.get('id') as string
    const parsedId: number | false = +id

    if (!parsedId) {
        this.alertService.push('מזהה תלמיד לא חוקי')
        this.router.navigate(['/students'])
        return
    }

    this.getStudentData(parsedId as number)
  }

  public async onSubmit(): Promise<any> {
    const newStudent: Student = {
      ...this.student,
      ...this.formService.toObject(),
    }

    return this.studentDataProvider
      .updateItem(newStudent)
      .then((x) => {
        this.alertService.push('פרטי התלמיד עודכנו בהצלחה')
        this.router.navigate(['/students'])
        return Promise.resolve(x)
      })
      .catch((error) => {
        this.alertService.push('עדכון פרטי התלמיד נכשל')
        return Promise.reject(error)
      })
  }

  private getStudentData(id: number): Promise<Student> {
    return this.studentDataProvider
      .getItemById(id)
      .then((student) => {
        this.student = student
        this.formService.fromObject(this.student)
        return Promise.resolve(student)
      })
      .catch((error) => {
        this.alertService.push(`לא נמצא תלמיד בעל מזהה מספר ${id}`)
        this.router.navigate(['/students'])
        return Promise.reject(error)
      })
  }
}
