import { Injectable } from '@angular/core'
import { BaseStudentForm } from '@exp-features/students/components/student-data/base-students-form-service.class'
import { FormBuilder } from '@angular/forms'

@Injectable()
export class EditStudentFormService extends BaseStudentForm {
    constructor(formBuilder: FormBuilder) {
        super(formBuilder)
    }
}
