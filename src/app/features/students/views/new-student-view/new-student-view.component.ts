import { Component } from '@angular/core'
import { FormGroup, Validators, FormControl } from '@angular/forms'
import { Gender } from '@exp-app/core/models/enums/gender.enum'
import { StudentsDataProviderService } from '@exp-app/core/services/data-providers/students-data-provider.service'
import { Student } from '@exp-app/core/models/student'
import { AlertService } from '@exp-app/core/services/alert.service'
import { Router } from '@angular/router'
import { NewStudentFormService } from './new-student-form.service'

@Component({
  selector: 'exp-new-student-view',
  templateUrl: './new-student-view.component.html',
  styleUrls: ['./new-student-view.component.scss'],
  providers: [NewStudentFormService],
})
export class NewStudentViewComponent {
  constructor(
    private readonly studentDataProvider: StudentsDataProviderService,
    private readonly alertService: AlertService,
    private readonly router: Router,
    public readonly formService: NewStudentFormService,
  ) { }

  public async onSubmit(): Promise<any> {
    const student: Student = {
      id: 0,
      registrationDate: new Date(),
      ...this.formService.toObject(),
    }

    return this.studentDataProvider.addNewItem(student)
    .then(values => {
      this.alertService.push('התלמיד נוסף בהצלחה')
      this.router.navigate(['/students'])
      return Promise.resolve(values)
    })
    .catch(error => {
      this.alertService.push('לא היה ניתן להוסיף את התלמיד למערכת')
      return Promise.reject(error)
    })
  }
}
