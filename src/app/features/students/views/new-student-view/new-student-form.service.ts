import { Injectable } from '@angular/core'
import { FormBuilder } from '@angular/forms'
import { BaseStudentForm } from '@exp-features/students/components/student-data/base-students-form-service.class'

@Injectable()
export class NewStudentFormService extends BaseStudentForm {
    constructor(formBuilder: FormBuilder) {
        super(formBuilder)
    }
}
