import { NgModule } from '@angular/core'
import { CoreModule } from '@exp-core/core.module'
import { StudentsViewComponent } from './views/students-view/students-view.component'
import { NewStudentViewComponent } from './views/new-student-view/new-student-view.component'
import { EditStudentViewComponent } from './views/edit-student-view/edit-student-view.component'
import { StudentDataComponent } from './components/student-data/student-data.component'
import { StudentGridItemComponent } from './components/student-grid-item/student-grid-item.component'
import { ComponentsModule } from '@exp-app/components/components.module'
import { MaterialModule } from '@exp-material'
import { SpellChipsComponent } from './components/spell-chips/spell-chips.component'

@NgModule({
  declarations: [
    StudentsViewComponent,
    NewStudentViewComponent,
    EditStudentViewComponent,
    StudentDataComponent,
    StudentGridItemComponent,
    SpellChipsComponent,
  ],
  imports: [
    CoreModule,
    ComponentsModule,
    MaterialModule,
  ],
  exports: [
    StudentsViewComponent,
    NewStudentViewComponent,
    EditStudentViewComponent,
    SpellChipsComponent,
  ],
})
export class StudentsModule {}
