import { Injectable } from '@angular/core'
import { BaseSimpleFilterService } from '@exp-core/services/base-simple-filter.service.class'
import { Spell } from '@exp-app/core/models/spell'
import { FormBuilder } from '@angular/forms'

@Injectable()
export class SpellsViewFilterService extends BaseSimpleFilterService<Spell> {
    constructor(formBuilder: FormBuilder) {
        super(formBuilder)
    }

    protected filterBy(value: Spell, query: string): boolean {
        return value?.name?.includes(query) || value?.description?.includes(query)
    }
}
