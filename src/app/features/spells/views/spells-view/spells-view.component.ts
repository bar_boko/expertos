import { Component, OnInit } from '@angular/core'
import { SpellsDataProviderService } from '@exp-app/core/services/data-providers/spells-data-provider.service'
import { AlertService } from '@exp-app/core/services/alert.service'
import { SpellsViewFilterService } from './spells-view-filter.service'
import { Observable } from 'rxjs'
import { Spell } from '@exp-app/core/models/spell'

@Component({
  selector: 'exp-spells-view',
  templateUrl: './spells-view.component.html',
  styleUrls: ['./spells-view.component.scss'],
  providers: [SpellsViewFilterService],
})
export class SpellsViewComponent implements OnInit {
  public readonly tableColumns = ['id', 'name', 'description']
  public filteredSpells: Observable<Spell[]>

  constructor(
    private readonly dataProvider: SpellsDataProviderService,
    private readonly alertService: AlertService,
    public readonly filtersService: SpellsViewFilterService,
  ) {}

  public ngOnInit(): void {
    this.dataProvider
      .getItems()
      .then(result => this.filtersService.setValues(result))
      .catch(() =>
        this.alertService.push('חלה שגיאה בקבלת רשימת הכשפים מהמערכת'),
      )

    this.filteredSpells = this.filtersService.filteredValues
  }
}
