import { Injectable } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Spell } from '@exp-app/core/models/spell'
import { BaseFormService } from '@exp-core/services/base-form-service.class'

const { minLength, maxLength, required } = Validators

export const NAME = 'name'
export const DESCRIPTION = 'description'

@Injectable()
export class NewSpellFormService extends BaseFormService<Spell> {
    public get name(): string {
        return this.getValue(NAME)
    }

    public get description(): string {
        return this.getValue(DESCRIPTION)
    }

    public constructor(formBuilder: FormBuilder) {
        super(formBuilder)
    }

    public fromObject(data: Spell): void {
        const { name, description } = data

        this.formGroup.patchValue({
            name, description,
        })
    }

    public toObject(): Spell {
        return {
            ...this.formGroup.value,
            id: 0,
        }
    }

    protected createFormGroup(): FormGroup {
        return this.formBuilder.group({
            [NAME]: ['', [required, minLength(2), maxLength(25)]],
            [DESCRIPTION]: ['', [required, minLength(3), maxLength(300)]],
        }, {updateOn: 'blur'})
    }
}
