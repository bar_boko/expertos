import { Component } from '@angular/core'
import { SpellsDataProviderService } from '@exp-app/core/services/data-providers/spells-data-provider.service'
import { AlertService } from '@exp-app/core/services/alert.service'
import { Router } from '@angular/router'
import { Spell } from '@exp-app/core/models/spell'
import { NewSpellFormService } from './new-spell-form.service'

@Component({
  selector: 'exp-new-spell-view',
  templateUrl: './new-spell-view.component.html',
  styleUrls: ['./new-spell-view.component.scss'],
  providers: [NewSpellFormService],
})
export class NewSpellViewComponent {
  constructor(
    private readonly dataProvider: SpellsDataProviderService,
    public readonly formService: NewSpellFormService,
    private readonly alertService: AlertService,
    private readonly router: Router,
  ) { }

  public async onSubmit(): Promise<any> {
    const newSpell: Spell = this.formService.toObject()

    return this.dataProvider.addNewItem(newSpell)
      .then(values => {
        this.alertService.push('הכישוף נוסף בהצלחה למערכת')
        this.router.navigate(['/spells'])
        return Promise.resolve(values)
      })
      .catch(error => {
        this.alertService.push('לא היה ניתן להוסיף את הכישוף למערכת')
        return Promise.reject(error)
      })
  }
}
