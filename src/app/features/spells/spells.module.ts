import { NgModule } from '@angular/core'
import { SpellsViewComponent } from './views/spells-view/spells-view.component'
import { NewSpellViewComponent } from './views/new-spell-view/new-spell-view.component'
import { CoreModule } from 'src/app/core/core.module'
import { ComponentsModule } from 'src/app/components/components.module'
import { MaterialModule } from '@exp-material'

@NgModule({
  declarations: [
    SpellsViewComponent,
    NewSpellViewComponent,
  ],
  imports: [
    CoreModule,
    ComponentsModule,
    MaterialModule,
  ],
  exports: [SpellsViewComponent, NewSpellViewComponent],
})
export class SpellsModule {}
