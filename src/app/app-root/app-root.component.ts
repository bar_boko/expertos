import { Component, OnInit } from '@angular/core'
import { environment } from '@exp-env'

@Component({
  selector: 'exp-app-root',
  templateUrl: './app-root.component.html',
  styleUrls: ['./app-root.component.scss'],
})
export class AppRootComponent {
  public readonly direction: string

  constructor() {
    this.direction = environment.langDirection
  }
}
