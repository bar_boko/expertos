# Reactive Forms

## Creating the Binding

```typescript
import { FormGroup, FormControl } from "@angular/forms";

@Component({})
export class ExampleComponent {
  public readonly filtersForm: FormGroup = new FormGroup({
    query: new FormControl(""),
    group: new FormGroup({
      a: new FormControl(""),
      b: new FormControl("")
    })
  });
}
```

## HTML
```html
<input type="text" formControlName="query" />
<div formGroupName="group">
    <input type="text" formControlName="a">
    <input type="text" formControlName="b">
</div>
<button type="submit" [disabled]="!filtersForm.valid">Submit</button>
```