# Observable Binding

## Component TypeScript
```typescript
import { Component } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay, share } from 'rxjs/operators';

@Component({
  selector: 'demo-app',
  templateUrl: 'app/app.component.html'
})
export class AppComponent {
  user: Observable<{}>;

  constructor() {}

  ngOnInit() {
    this.user = this.getAsyncData().pipe(share());
  }

  getAsyncData() {
    // Fake Slow Async Data
    return of({
      firstName: 'Luke',
      lastName: 'Skywalker',
      age: 65,
      height: 172,
      mass: 77,
      homeworld: 'Tatooine'
    }).pipe(delay(2000));
  }
}
```

# Template
```html
<h2>Async Pipe and share()</h2>
<div>
  <h2>{{(user | async)?.firstName}} {{(user | async)?.lastName}}</h2>
  <dl>
    <dt>Age:</dt>
    <dd>{{(user | async)?.age}}</dd>

    <dt>Height:</dt>
    <dd>{{(user | async)?.height}}</dd>

    <dt>Mass:</dt>
    <dd>{{(user | async)?.mass}}</dd>

    <dt>Home World:</dt>
    <dd>{{(user | async)?.homeWorld}}</dd>
  </dl>
</div>
```