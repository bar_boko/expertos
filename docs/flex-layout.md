# Flex Layout
## Sizes
Size     | Range
---------|------------
**XS**   | `[0, 600)`
**SM**   | `[600, 960)`
**MD**   | `[960, 1280)`
**LG**   | `[1280, 1920)`
**XL**   | `[1920, 5000]`

# Queries of `fxFlex`
Assume that all of our `<div>` have the directive `fxFlex`

33% fxFlex | 50% fxFlex | XS | SM | MD | LG
-----------|------------|----|----|----|----
`gt-xs`    | `gt-sm`    | A  | 33 | 50 | 50
`lt-sm`    | `lt-lg`    | 33 | 50 | 50 | A
`gt-sm`    | `lt-lg`    | 50 | 50 | 50 | 33
